import { StyleSheet } from 'react-native'

export const LIGHT_BLUE = "#82ccdd";
export const DARK_BLUE = "#0073b0";

export const Style = StyleSheet.create({
    mainWindow: {
        flex: 1,
        backgroundColor: LIGHT_BLUE
    },
    container: {
        backgroundColor: LIGHT_BLUE,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    }
});