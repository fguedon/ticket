import { StyleSheet } from 'react-native'

const CameraScannerStyle = StyleSheet.create({
    container: {
        maxHeight: 200,
        overflow: 'hidden'
    },
    preview: {
        height: 200
    }
});

module.exports = CameraScannerStyle;