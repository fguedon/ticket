import React from 'react';
import {
    createSwitchNavigator,
    createAppContainer,
    createMaterialTopTabNavigator
} from 'react-navigation';

import Icon from "react-native-vector-icons/Ionicons";

import {DARK_BLUE} from '../assets/style/style';

import AuthLoadingScreen from "./screens/AuthLoading";
import HomeScreen from "./screens/SignedIn/Home";
import OtherScreen from "./screens/SignedIn/Other";
import SignInScreen from "./screens/SignedOut/SignIn";
import SignUpScreen from "./screens/SignedOut/SignUp";

const tabOptions = {
    swipeEnabled: true,
    tabBarPosition: 'bottom',
    tabBarOptions: {
        activeTintColor: '#fff',
        inactiveTintColor: '#000',
        style: {
            backgroundColor: DARK_BLUE
        },
        indicatorStyle: {
            height: 2,
            backgroundColor: '#fff'
        },
        showLabel: false,
        showIcon: true
    }
};

const AuthStack = createMaterialTopTabNavigator({
    SignIn: {
        screen: SignInScreen,
        navigationOptions: {
            title: 'Sign in',
            tabBarIcon: ({tintColor}) => (
                <Icon name="ios-log-in" color={tintColor} size={24}/>
            )
        }
    },
    SignUp: {
        screen: SignUpScreen,
        navigationOptions: {
            title: 'Sign up',
            tabBarIcon: ({tintColor}) => (
                <Icon name="ios-person-add" color={tintColor} size={24}/>
            )
        }
    },
}, tabOptions);

const AppStack = createMaterialTopTabNavigator({
    Home: HomeScreen,
    Other: OtherScreen
}, tabOptions);

export default createAppContainer(createSwitchNavigator(
    {
        AuthLoading: AuthLoadingScreen,
        App: AppStack,
        Auth: AuthStack,
    }, {
        initialRouteName: 'AuthLoading',
    }
));