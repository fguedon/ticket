import React from 'react'
import {AsyncStorage, Button, View} from 'react-native';
import {Style as styles} from '../../../assets/style/style';

export default class SignInScreen extends React.Component {
    _signInAsync = async () => {
        await AsyncStorage.setItem('userToken', 'abc');
        this.props.navigation.navigate('App');
    };

    render() {
        return (
            <View style={styles.container}>
                <Button title="Sign in!" onPress={this._signInAsync}/>
            </View>
        );
    }
}