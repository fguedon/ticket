import React from "react";
import {AsyncStorage, Button, View} from "react-native";
import {Style as styles} from '../../../assets/style/style';

export default class SignUpScreen extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Button title="Sign up!" onPress={this._signUpAsync} />
            </View>
        );
    }

    _signUpAsync = async () => {
        await AsyncStorage.setItem('userToken', 'abc');
        this.props.navigation.navigate('App');
    };
}