'use strict';
import React, {Component} from 'react';
import {AppRegistry, Vibration, View} from 'react-native';
import {RNCamera} from 'react-native-camera';
import {Conf} from "../conf";
import {CameraStyle} from "../assets/style";

export default class CameraScanner extends Component {

    static navigationOptions = {
        title: 'Trololo'
    };

    constructor(props) {
        super(props);
    }

    _onBarCodeRead(codeBar) {
        Vibration.vibrate(Conf.SUCCESS_SCAN_VIBRATE_DURATION);
        console.log((new Date()).getTime() + '//' + codeBar.data + ' // ' + codeBar.type);
    }

    render() {
        return (
            <View style={CameraStyle.container}>
                <RNCamera
                    ref={ref => {
                        this.camera = ref;
                    }}
                    style={CameraStyle.preview}
                    type={RNCamera.Constants.Type.back}
                    flashMode={RNCamera.Constants.FlashMode.off}
                    permissionDialogTitle={'Permission to use camera'}
                    permissionDialogMessage={'We need your permission to use your camera phone'}
                    onBarCodeRead={this._onBarCodeRead}
                />
            </View>
        );
    }
}

AppRegistry.registerComponent('CameraScanner', () => CameraScanner);