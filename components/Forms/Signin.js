import React from 'react';
import {AppRegistry, Button, Image, StyleSheet, Text, TextInput, View} from 'react-native';
import {Global} from '../../assets/style';

export default class SigninForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        };
    }

    _onSubmit() {
        console.log(this.state);
    }

    render() {
        return (
            <View style={Global.mainWindow}>
                <View style={{flex: 10, justifyContent: 'center', paddingHorizontal: 10}}>

                    {/*<Image*/}
                    {/*style={{width: 128, height: 128}}*/}
                    {/*source={{uri: 'https://tbncdn.freelogodesign.org/c0eb6bee-5c04-4a4c-b8f9-1580166d947a.png'}}*/}
                    {/*/>*/}

                    <View style={styles.group}>
                        <Text style={styles.label}>Email</Text>
                        <TextInput
                            autoCorrect={false}
                            spellCheck={false}
                            autoCapitalize='none'
                            keyboardType='email-address'
                            style={styles.input}
                            underlineColorAndroid='white'
                            onChangeText={(email) => {
                                this.setState({email})
                            }}
                            value={this.state.email}
                        />
                    </View>
                    <View style={styles.group}>
                        <Text style={styles.label}>Mot de passe</Text>
                        <TextInput
                            autoCorrect={false}
                            spellCheck={false}
                            autoCapitalize='none'
                            secureTextEntry={true}
                            textContentType='password'
                            content='email-address'
                            style={styles.input}
                            underlineColorAndroid='white'
                            onChangeText={(password) => {
                                this.setState({password})
                            }}
                            value={this.state.password}
                        />
                    </View>
                    <View style={{paddingHorizontal: 50}}>
                        <Button
                            title="Connexion"
                            onPress={this._onSubmit.bind(this)}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    input: {
        flex: 7,
        height: 40,
        margin: 0,
        color: 'white',
        padding: 5,
    },
    label: {
        fontSize: 18,
        height: 40,
        lineHeight: 40,
        paddingRight: 6,
        color: 'white'
    },
    group: {
        flex: 0,
        height: 40,
        flexDirection: 'row',
        alignContent: 'center',
        alignItems: 'stretch',
        marginBottom: 10,
        paddingHorizontal: 20
    }
});

AppRegistry.registerComponent('SigninForm', () => SigninForm);